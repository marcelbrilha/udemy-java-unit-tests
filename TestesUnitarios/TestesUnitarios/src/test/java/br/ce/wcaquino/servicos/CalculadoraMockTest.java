package br.ce.wcaquino.servicos;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class CalculadoraMockTest {
	
	@Mock
	private Calculadora calcMock;
	
	@Spy
	private Calculadora calcSpy; // Por padrão executa o método, não funciona com interfaces
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void test() {
		Calculadora calc = Mockito.mock(Calculadora.class);
		
		// Quando você usar um matcher, todos os parâmetros devem ser matchers, para fixar um valor deve-se usar o eq
		Mockito.when(calc.somar(Mockito.eq(1), Mockito.anyInt())).thenReturn(5);
				
		ArgumentCaptor<Integer> argCapt = ArgumentCaptor.forClass(Integer.class);
		Mockito.when(calc.somar(argCapt.capture(), argCapt.capture())).thenReturn(5);
		
		System.out.println(calc.somar(1, 50));
		System.out.println(argCapt.getAllValues());
	}
	
	@Test
	public void testsSpyAndMock() {
		// Mockito.when(calcMock.somar(1,  2)).thenCallRealMethod(); // Executa o método
		Mockito.when(calcMock.somar(1,  2)).thenReturn(8);
		Mockito.when(calcSpy.somar(1,  2)).thenReturn(8);
//		Mockito.doReturn(5).when(calcSpy).somar(1, 2); // Não vai executar o método internamente
//		Mockito..doNothing().when(calcSpy).imprime(); // Não executa por padrão - método void
		
		System.out.println(calcMock.somar(1, 3)); // Faz um mock do valor, como é inteiro retorna 0
		System.out.println(calcSpy.somar(1, 3)); // Executa o metodo - retorna 4 - devido as expectativas serem diferentes
	}
}
