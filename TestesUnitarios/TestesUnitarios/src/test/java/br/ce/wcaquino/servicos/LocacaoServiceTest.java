package br.ce.wcaquino.servicos;

import static br.ce.wcaquino.builders.UsuarioBuilder.umUsuario;
import static br.ce.wcaquino.matchers.MatchersProprios.caiEm;
import static br.ce.wcaquino.matchers.MatchersProprios.caiNumaSegunda;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ce.wcaquino.builders.FilmeBuilder;
import br.ce.wcaquino.builders.UsuarioBuilder;
import br.ce.wcaquino.daos.LocacaoDAO;
import br.ce.wcaquino.entidades.Filme;
import br.ce.wcaquino.entidades.Locacao;
import br.ce.wcaquino.entidades.Usuario;
import br.ce.wcaquino.exception.LocadoraException;
import br.ce.wcaquino.utils.DataUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest(LocacaoService.class)
// @PrepareForTest({LocacaoService.class, DataUtils.class}) // Exemplo powermock com mais de uma classe
public class LocacaoServiceTest {

	@Rule
	public ErrorCollector error = new ErrorCollector();

	@Rule
	public ExpectedException exception = ExpectedException.none();

	private final String messageException = "Filme sem estoque";

	@InjectMocks
	private LocacaoService locacaoService;

	private List<Filme> filmes = new ArrayList<>();

	@Mock
	private LocacaoDAO dao;

	@Mock
	private SPCService spcService;

	@Mock
	private EmailService emailService;

	// Escopo da classe = JUnit não vai reinicializar por teste
	private static int contador = 0;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		// locacaoService = PowerMockito.spy(locacaoService); Criar spy do powermock
		contador++;
		System.out.println(contador);
	}

	@After
	public void tearDown() {
		System.out.println("After");
	}

	@BeforeClass
	public static void setupClass() {
		System.out.println("@BeforeClass");
	}

	@AfterClass
	public static void tearDownCLass() {
		System.out.println("@AfterClass");
	}

	@Test
	public void deveAlugarFilme() throws Exception {
		Assume.assumeFalse(DataUtils.verificarDiaSemana(new Date(), Calendar.SATURDAY));

		// Cenário
		Usuario usuario = umUsuario().agora();
		filmes.add(new Filme("Up - Altas Aventuras", 2, 5.0));

		// Ação
		Locacao locacao = locacaoService.alugarFilme(usuario, filmes);

		// Resultado
		Assert.assertEquals(5.0, locacao.getValor(), 0.01);
		Assert.assertTrue(DataUtils.isMesmaData(locacao.getDataLocacao(), new Date()));
		Assert.assertTrue(DataUtils.isMesmaData(locacao.getDataRetorno(), DataUtils.obterDataComDiferencaDias(1)));

		Assert.assertThat(locacao.getValor(), CoreMatchers.is(5.0));
		error.checkThat(locacao.getValor(), is(equalTo(5.0)));
		error.checkThat(locacao.getValor(), is(not(6.0)));

		error.checkThat(DataUtils.isMesmaData(locacao.getDataLocacao(), new Date()), is(true));
		error.checkThat(DataUtils.isMesmaData(locacao.getDataRetorno(), DataUtils.obterDataComDiferencaDias(1)),
				is(true));
	}

	@Test(expected = Exception.class)
	public void testLocacaoFilmeSemEstoqueElegante() throws Exception {
		// Cenário
		Usuario usuario = new Usuario("Marcel");
		filmes.add(new Filme("Up - Altas Aventuras", 0, 5.0));

		// Ação
		locacaoService.alugarFilme(usuario, filmes);
	}

	@Test
	public void testLocacaoFilmeSemEstoqueRobusta() {
		// Cenário
		Usuario usuario = umUsuario().agora();
		filmes.add(new Filme("Up - Altas Aventuras", 0, 5.0));

		// Ação
		try {
			locacaoService.alugarFilme(usuario, filmes);
			Assert.fail("Deveria ter lançado uma exceção - Filme sem estoque"); // Evitando falso positivo
		} catch (Exception e) {
			assertThat(e.getMessage(), is(messageException));
		}
	}

	@Test
	public void testLocacaoFilmeSemEstoqueENovaForma() throws Exception {
		// Cenário
		Usuario usuario = umUsuario().agora();
		filmes.add(new Filme("Up - Altas Aventuras", 0, 5.0));

		exception.expect(Exception.class);
		exception.expectMessage(messageException);

		// Ação
		locacaoService.alugarFilme(usuario, filmes);

	}

	@Test
	public void testLocacaoUsuarioVazio() throws Exception {
		// Cenário
		filmes.add(new Filme("Up - Altas Aventuras", 2, 5.0));

		exception.expect(LocadoraException.class);
		exception.expectMessage("Usuário vazio");

		// Ação
		locacaoService.alugarFilme(null, filmes);

	}

	@Test
	public void testLocacaoFilmeVazio() throws Exception {
		// Cenário
		Usuario usuario = umUsuario().agora();

		exception.expect(LocadoraException.class);
		exception.expectMessage("Filme vazio");

		// Ação
		locacaoService.alugarFilme(usuario, null);

	}

	@Test
	public void devePagar75PctNoFilme3() throws Exception {
		Usuario usuario = umUsuario().agora();
		filmes = Arrays.asList(new Filme("Filme 1", 2, 4.0), new Filme("Filme 2", 2, 4.0),
				new Filme("Filme 3", 2, 4.0));

		Locacao locacao = locacaoService.alugarFilme(usuario, filmes);

		assertThat(locacao.getValor(), is(11.0));

	}

	@Test
	public void devePagar50PctNoFilme4() throws Exception {
		Usuario usuario = umUsuario().agora();
		filmes = Arrays.asList(new Filme("Filme 1", 2, 4.0), new Filme("Filme 2", 2, 4.0), new Filme("Filme 3", 2, 4.0),
				new Filme("Filme 4", 2, 4.0));

		Locacao locacao = locacaoService.alugarFilme(usuario, filmes);

		assertThat(locacao.getValor(), is(13.0));

	}

	@Test
	public void devePagar25PctNoFilme5() throws Exception {
		Usuario usuario = umUsuario().agora();
		filmes = Arrays.asList(new Filme("Filme 1", 2, 4.0), new Filme("Filme 2", 2, 4.0), new Filme("Filme 3", 2, 4.0),
				new Filme("Filme 4", 2, 4.0), new Filme("Filme 5", 2, 4.0));

		Locacao locacao = locacaoService.alugarFilme(usuario, filmes);

		assertThat(locacao.getValor(), is(14.0));

	}

	@Test
	public void devePagar0NoFilme6() throws Exception {
		Usuario usuario = umUsuario().agora();
		filmes = Arrays.asList(new Filme("Filme 1", 2, 4.0), new Filme("Filme 2", 2, 4.0), new Filme("Filme 3", 2, 4.0),
				new Filme("Filme 4", 2, 4.0), new Filme("Filme 5", 2, 4.0), new Filme("Filme 6", 2, 4.0));

		Locacao locacao = locacaoService.alugarFilme(usuario, filmes);

		assertThat(locacao.getValor(), is(14.0));

	}

	@Test
	// @Ignore - Ignorar o teste
	public void deveDevolverNaSegundaAoAlugarNoSabado() throws Exception {
		// Executa somente quando for sábado - Assumptions
		// Assume.assumeTrue(DataUtils.verificarDiaSemana(new Date(),
		// Calendar.SATURDAY));

		Usuario usuario = umUsuario().agora();
		filmes = Arrays.asList(FilmeBuilder.umFilme().agora());

		// Mockando construtor de Date()
		PowerMockito.whenNew(Date.class).withNoArguments().thenReturn(DataUtils.obterData(29, 4, 2017));

		Locacao locacao = locacaoService.alugarFilme(usuario, filmes);
		boolean segunda = DataUtils.verificarDiaSemana(locacao.getDataRetorno(), Calendar.MONDAY);

		Assert.assertTrue(segunda);
//		assertThat(locacao.getDataRetorno(), new DiaSemanaMatcher(Calendar.MONDAY));
		assertThat(locacao.getDataRetorno(), caiEm(Calendar.MONDAY));
		assertThat(locacao.getDataRetorno(), caiNumaSegunda(Calendar.MONDAY));

		// Verifica se construtor de date foi chamador duas vezes
		PowerMockito.verifyNew(Date.class, Mockito.times(2)).withNoArguments();
	}

	@Test
	public void deveLancarExceptionUsuarioNegativado() throws Exception {
		// Cenário
		Usuario usuario = umUsuario().agora();
		filmes.add(new Filme("Up - Altas Aventuras", 2, 5.0));

		Mockito.when(spcService.possuiNegativacao(usuario)).thenReturn(true);

		exception.expect(LocadoraException.class);
		exception.expectMessage("Usuário negativado");

		// Ação
		locacaoService.alugarFilme(usuario, filmes);

		// Verificacao
		verify(spcService).possuiNegativacao(usuario);
	}

	@Test
	public void deveLancarExceptionUsuarioNegativado2() {
		// Cenário
		Usuario usuario = umUsuario().agora();
		filmes.add(new Filme("Up - Altas Aventuras", 2, 5.0));

		Mockito.when(spcService.possuiNegativacao(usuario)).thenReturn(true);

		try {
			// Verificacao
			locacaoService.alugarFilme(usuario, filmes);
			Assert.fail(); // Caso não lance a exceção não gera falso positivo
		} catch (LocadoraException e) {
			Assert.assertThat(e.getMessage(), is("Usuário negativado"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		verify(spcService).possuiNegativacao(usuario);
	}

	@Test
	public void deveEnviarEmailLocacoesAtrasadas() {
		// Cenário
		Usuario usuario = UsuarioBuilder.umUsuario().agora();
		List<Locacao> locacoes = Arrays.asList(new Locacao(usuario, new Date(), DataUtils.obterDataComDiferencaDias(-2),
				4.0, Arrays.asList(FilmeBuilder.umFilme().agora())));
		Mockito.when(dao.obterLocacoesPendentes()).thenReturn(locacoes);

		// Ação
		locacaoService.notificarAtrasos();

		// Verificação
		Mockito.verify(emailService).notificarAtraso(usuario);
		Mockito.verify(emailService, Mockito.times(1)).notificarAtraso(Mockito.any(Usuario.class));
//		Mockito.verify(emailService, Mockito.atLeast(2)).notificarAtraso(usuario);
//		Mockito.verify(emailService, Mockito.times(2)).notificarAtraso(usuario);
//		Mockito.verify(emailService, Mockito.never()).notificarAtraso(usuario);
//		Mockito.verifyNoMoreInteractions(emailService);
		Mockito.verifyZeroInteractions(spcService);

		// Lançar exceção
//		Mockito.when(spcService.possuiNegativacao(usuario)).thenThrow(new RuntimeException("Falha Catastrófica"));
	}

	@Test
	public void deveProrrogarLocacao() {
		// cenario
		Locacao locacao = new Locacao(UsuarioBuilder.umUsuario().agora(), new Date(), new Date(), 4.0,
				Arrays.asList(FilmeBuilder.umFilme().agora()));

		// acao
		locacaoService.prorrogarLocacao(locacao, 3);

		// Verificação
		ArgumentCaptor<Locacao> argCapt = ArgumentCaptor.forClass(Locacao.class);
		Mockito.verify(dao).salvar(argCapt.capture());
		Locacao locacaoRetornada = argCapt.getValue();

		error.checkThat(locacaoRetornada.getValor(), is(12.0));

	}

	@Test
	public void alugarFilme() throws Exception {
		// Cenário
		Usuario usuario = UsuarioBuilder.umUsuario().agora();

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 29);
		calendar.set(Calendar.MONTH, Calendar.APRIL);
		calendar.set(Calendar.YEAR, 2017);

		// Mockando método estático - getInstance do calendar
		PowerMockito.mockStatic(Calendar.class);
		PowerMockito.when(Calendar.getInstance()).thenReturn(calendar);

		// Ação
		locacaoService.alugarFilme(usuario, Arrays.asList(FilmeBuilder.umFilme().agora()));

		// Verificação
		// Verificando chamadas do método estático com powermockito
//		PowerMockito.verifyStatic(Mockito.times(2));
//		Calendar.getInstance();

	}
	
	@Test
	public void deveAlugarFilmeSemCalcularValor() throws Exception {
		Usuario usuario = umUsuario().agora();
		List<Filme> filmes = Arrays.asList(FilmeBuilder.umFilme().agora());
		
		Locacao locacao = locacaoService.alugarFilme(usuario, filmes);
		
		// Mock método privado
		// PowerMockito.doReturn(1.0).when(locacaoService, "calcularValorLocacao", filmes);
		
		// Forma de executar metodos privados sem powermock
//		Class<LocacaoService> clazz = LocacaoService.class;
//		Method metodo = clazz.getDeclaredMethod("calcularValorLocacao", List.class);
//		metodo.setAccessible(true);
//		Double valor = (Double) metodo.invoke(locacaoService, filmes);
		
		Assert.assertThat(locacao.getValor(), is(4.0));
		
		// Verificação método privado diretamente
		// PowerMockito.verifyPrivate(locacaoService).invoke("calcularValorLocacao", filmes);
		
		// Executando método privado - Whitebox deve ser do powermock, o do mockito não funciona para metodos privados
		// Double valor = (Double) Whitebox.invokeMethod(locacaoService, "calcularValorLocacao", filmes);
	}
}
