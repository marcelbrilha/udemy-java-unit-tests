package br.ce.wcaquino.servicos;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.ce.wcaquino.exception.DividirPorZeroException;

// @RunWith(ParallelRunner.class) - Exemplo de como usar a classe Runner ParallelRunner
public class CalculadoraTest {

	private Calculadora calculadora;

	@Before
	public void setup() {
		calculadora = new Calculadora();
	}

	@Test
	public void deveSomarDoisValores() {
		int a = 5;
		int b = 3;

		int resultado = calculadora.somar(a, b);

		Assert.assertEquals(8, resultado);
	}

	@Test
	public void deveSubtrairDoisValores() {
		int a = 8;
		int b = 5;

		int resultado = calculadora.subtrair(a, b);
		Assert.assertEquals(3, resultado);
	}

	@Test
	public void deveDividirDoisValores() throws DividirPorZeroException {
		int a = 9;
		int b = 3;

		int resultado = calculadora.dividir(a, b);
		Assert.assertEquals(3, resultado);
	}

	@Test(expected = DividirPorZeroException.class)
	public void exceptionDividirPorZero() throws DividirPorZeroException {
		int a = 9;
		int b = 0;

		int resultado = calculadora.dividir(a, b);
		Assert.assertEquals(3, resultado);
	}
}
