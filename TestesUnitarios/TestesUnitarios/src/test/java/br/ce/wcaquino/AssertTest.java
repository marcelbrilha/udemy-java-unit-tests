package br.ce.wcaquino;

import org.junit.Assert;
import org.junit.Test;

import br.ce.wcaquino.entidades.Usuario;

public class AssertTest {

	@Test
	public void test() {
		Assert.assertTrue(true);
		Assert.assertFalse(false);
		
		Assert.assertEquals(2, 2);
		Assert.assertEquals(2.25, 2.25, 0.01);
		Assert.assertEquals(Math.PI, 3.14, 0.01);
		
		int i = 5;
		Integer i2 = 5;
		
		Assert.assertEquals(Integer.valueOf(i), i2);
		Assert.assertEquals(i, i2.intValue());
		
		Assert.assertEquals("teste", "teste");
		Assert.assertNotEquals("teste", "teste2");
		Assert.assertTrue("teste".equalsIgnoreCase("Teste"));
		Assert.assertTrue("teste".startsWith("tes"));
		
		Usuario u1 = new Usuario("Marcel");
		Usuario u2 = new Usuario("Marcel");
		Usuario u3 = null;
		
		Assert.assertEquals(u1, u2);
		Assert.assertSame(u2, u2);
		Assert.assertNotSame(u1, u2);
		Assert.assertNull(u3);
		Assert.assertNotNull(u2);
		
		Assert.assertEquals("Erro de comparação", "teste", "teste");
	}
}
