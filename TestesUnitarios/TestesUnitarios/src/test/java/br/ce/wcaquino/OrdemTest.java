package br.ce.wcaquino;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING) // Garantir que os métodos estejam em ordem alfabética
public class OrdemTest {

	private static int contador = 0;

	@Test
	public void inicia() {
		contador = 1;
	}

	@Test
	public void verifica() {
		Assert.assertEquals(1, contador);
	}
	
//	@Test Por Padrão o JUnit dessa forma não consegue garantir a ordem de execução dos testes
//	Essa forma não é muito legal - devido a não ter uma rastreabilidade
//	@Test
//	public void testGeral() {
//		inicia();
//		verifica();
//	}
}
