package br.ce.wcaquino.exception;

public class DividirPorZeroException extends Exception {

	private static final long serialVersionUID = 1L;

	public DividirPorZeroException(String message) {
		super(message);
	}

}
