package br.ce.wcaquino.servicos;

import br.ce.wcaquino.exception.DividirPorZeroException;

public class Calculadora {

	public int somar(int i, int j) {
		return i + j;
	}

	public int subtrair(int a, int b) {
		return a - b;
	}

	public int dividir(int a, int b) throws DividirPorZeroException {

		if (a == 0 || b == 0)
			throw new DividirPorZeroException("Erro ao dividir por 0");

		return a / b;
	}

}
