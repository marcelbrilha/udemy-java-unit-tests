package com.example.tdd.model;

import java.time.LocalDate;

public class BookingModel {

	private Long id;
	private String reserveName;
	private LocalDate checkIn;
	private LocalDate checkOut;
	private Integer numberGuest;

	public BookingModel() {
	}

	public BookingModel(Long id, String reserveName, LocalDate checkIn, LocalDate checkOut, Integer numberGuest) {
		this.id = id;
		this.reserveName = reserveName;
		this.checkIn = checkIn;
		this.checkOut = checkOut;
		this.numberGuest = numberGuest;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReserveName() {
		return reserveName;
	}

	public void setReserveName(String reserveName) {
		this.reserveName = reserveName;
	}

	public LocalDate getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(LocalDate checkIn) {
		this.checkIn = checkIn;
	}

	public LocalDate getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(LocalDate checkOut) {
		this.checkOut = checkOut;
	}

	public Integer getNumberGuest() {
		return numberGuest;
	}

	public void setNumberGuest(Integer numberGuest) {
		this.numberGuest = numberGuest;
	}

}
