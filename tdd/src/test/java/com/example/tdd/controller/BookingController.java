package com.example.tdd.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.example.tdd.model.BookingModel;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
public class BookingController {

	@Autowired
	MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;
	
	private final String PATH = "/bookings";

	@Test
	public void bookingTestGetAll() throws Exception {
		mockMvc.perform(get(PATH)).andExpect(status().isOk());
	}

	@Test
	public void bookingTestSave() throws Exception {
		LocalDate checkIn = LocalDate.parse("2020-11-10");
		LocalDate checkOut = LocalDate.parse("2020-11-20");
		BookingModel bookingModel = new BookingModel(new Long(1), "Marcel", checkIn, checkOut, 2);

		mockMvc.perform(post(PATH).contentType("application/json")
				.content(objectMapper.writeValueAsString(bookingModel))).andExpect(status().isCreated());
	}
}
